from typing import Self


class Hand:
    allowed_cards = '23456789TJQKA'

    def __init__(self, cards: str):
        if not all(card in self.allowed_cards for card in cards):
            raise ValueError(f'Invalid card in {cards}')
        if len(cards) != 5:
            raise ValueError(f'Invalid length of {cards}')
        self.cards = cards

    def __str__(self):
        return self.cards

    @property
    def card_amounts(self) -> dict[str, int]:
        result = {}
        for allowed in self.allowed_cards:
            result[allowed] = sum(allowed == card for card in self.cards)
        return result

    @property
    def _card_powers(self):
        return [self.allowed_cards.index(card) for card in self.cards]

    def getwith(self, amount: int) -> list[str]:
        return [card for card, amt in self.card_amounts.items() if amount == amt]

    @property
    def fiveofakind(self) -> str | None:
        if c := self.getwith(5):
            return c[0]

    @property
    def fourofakind(self) -> str | None:
        if c := self.getwith(4):
            return c[0]

    @property
    def fullhouse(self) -> tuple[str, str] | None:
        if (c3 := self.getwith(3)) and (c2 := self.getwith(2)):
            return c3[0], c2[0]

    @property
    def threeofakind(self) -> str | None:
        if c := self.getwith(3):
            return c[0]

    @property
    def twopair(self) -> tuple[str, str] | None:
        if len(c := self.getwith(2)) == 2:
            return c[0], c[1]

    @property
    def onepair(self) -> str | None:
        if len(c := self.getwith(2)) == 1 and not self.getwith(3):
            return c[0]

    @property
    def power(self):
        if self.fiveofakind:
            return 6
        if self.fourofakind:
            return 5
        if self.fullhouse:
            return 4
        if self.threeofakind:
            return 3
        if self.twopair:
            return 2
        if self.onepair:
            return 1
        return 0

    def __lt__(self, other: Self) -> int:
        if self.power != other.power:
            return self.power < other.power
        return self._card_powers < other._card_powers


class Hand2(Hand):
    allowed_cards = 'J23456789TQKA'

    @property
    def card_amounts_nojoker(self) -> dict[str, int]:
        return {card: amt for card, amt in self.card_amounts.items() if card != 'J'}

    @property
    def fiveofakind(self) -> str | None:
        max_card, max_val = max(self.card_amounts_nojoker.items(), key=lambda x: x[1])
        if max_val + self.card_amounts['J'] >= 5:
            return max_card

    @property
    def fourofakind(self) -> str | None:
        max_card, max_val = max(self.card_amounts_nojoker.items(), key=lambda x: x[1])
        if max_val + self.card_amounts['J'] >= 4:
            return max_card

    @property
    def fullhouse(self) -> tuple[str, str] | None:
        max_card, max_val = max(self.card_amounts_nojoker.items(), key=lambda x: x[1])
        max2_card, max2_val = max(
            {card: amt for card, amt in self.card_amounts_nojoker.items() if card != max_card}.items(),
            key=lambda x: x[1])
        if 5 - max_val - max2_val <= self.card_amounts['J']:
            return max_card, max2_card

    @property
    def threeofakind(self) -> str | None:
        max_card, max_val = max(self.card_amounts_nojoker.items(), key=lambda x: x[1])
        if max_val + self.card_amounts['J'] >= 3:
            return max_card

    @property
    def twopair(self) -> tuple[str, str] | None:
        max_card, max_val = max(self.card_amounts_nojoker.items(), key=lambda x: x[1])
        max2_card, max2_val = max(
            {card: amt for card, amt in self.card_amounts_nojoker.items() if card != max_card}.items(),
            key=lambda x: x[1])
        if 4 - max_val - max2_val <= self.card_amounts['J']:
            return max_card, max2_card

    @property
    def onepair(self) -> str | None:
        max_card, max_val = max(self.card_amounts_nojoker.items(), key=lambda x: x[1])
        if max_val + self.card_amounts['J'] >= 2:
            return max_card


def parse_line(line: str) -> tuple[Hand, int]:
    line = line.split(' ')
    return Hand(line[0]), int(line[1])


def parse_line2(line: str) -> tuple[Hand2, int]:
    line = line.split(' ')
    return Hand2(line[0]), int(line[1])


def main1():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    lines = [parse_line(line) for line in data if line]
    lines = dict(lines)
    ranked_hands = sorted(lines.keys())
    lines = {hand: (ranked_hands.index(hand) + 1) * bid for hand, bid in lines.items()}
    print(sum(lines.values()))


def main2():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    lines = [parse_line2(line) for line in data if line]
    lines = dict(lines)
    ranked_hands = sorted(lines.keys())
    lines = {hand: (ranked_hands.index(hand) + 1) * bid for hand, bid in lines.items()}
    print(sum(lines.values()))


if __name__ == '__main__':
    main2()
