import re


def get_data(time: str, distance: str) -> dict[int, int]:
    time = re.sub(r' +', ' ', time)
    time = re.search(r'(?: \d+)+', time).group(0).strip().split(' ')
    time = [int(x) for x in time]
    distance = re.sub(r' +', ' ', distance)
    distance = re.search(r'(?: \d+)+', distance).group(0).strip().split(' ')
    distance = [int(x) for x in distance]
    return {t: d_t for t, d_t in zip(time, distance)}


def get_data2(time: str, distance: str) -> dict[int, int]:
    time = re.sub(r' +', '', time)
    time = re.search(r'\d+', time).group(0).strip().split(' ')
    time = [int(x) for x in time]
    distance = re.sub(r' +', '', distance)
    distance = re.search(r'\d+', distance).group(0).strip().split(' ')
    distance = [int(x) for x in distance]
    return {t: d_t for t, d_t in zip(time, distance)}


def get_distances(timer: int) -> dict[int, int]:
    return {t: t * timer - t ** 2 for t in range(timer + 1)}


def main1():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    records = get_data(data[0], data[1])
    res = 1
    win_amounts = {}
    for time, record in records.items():
        distances = get_distances(time)
        win_amounts[time] = sum(d > record for d in distances.values())
        res *= win_amounts[time]
    print(res)


def main2():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    time, record = list(get_data2(data[0], data[1]).items())[0]
    print(sum(d > record for d in get_distances(time).values()))


if __name__ == '__main__':
    main2()
