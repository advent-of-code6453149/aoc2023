import re


def parse_map(lines: list[str]) -> tuple[str, dict[str, tuple[str, str]]]:
    directions = lines[0]
    map = {}
    for line in lines[1:]:
        if not line:
            continue
        spot, left_way, right_way = re.match(r'(\w+) = \((\w+), (\w+)\)', line).group(1, 2, 3)
        map[spot] = left_way, right_way
    return directions, map


def next_steps(currs: list[str], map: dict[str, tuple[str, str]], direction: str) -> list[str]:
    nexts = []
    for curr in currs:
        nexts.append(map[curr][direction == 'R'])
    return nexts


def main1():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    directions, map = parse_map(data)
    instructions = list(directions)
    walk_index = 0
    curr = 'AAA'
    already_been = set()
    while curr != 'ZZZ':
        if not instructions:
            instructions = list(directions)
        next_instr = instructions.pop(0)
        if (curr, walk_index % len(directions)) in already_been:
            raise Exception()
        already_been.add((curr, walk_index))
        curr = map[curr][next_instr == 'R']
        walk_index += 1
    print(walk_index)


def main2():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    directions, map = parse_map(data)
    instructions = list(directions)
    currs = [p for p in map.keys() if p.endswith('A')]
    steps = 0
    while len(currs) != (arrived := sum(curr.endswith('Z') for curr in currs)):
        if not instructions:
            instructions = list(directions)
        next_instr = instructions.pop(0)
        currs = next_steps(currs, map, next_instr)
        steps += 1
    print(steps)


if __name__ == '__main__':
    main2()
