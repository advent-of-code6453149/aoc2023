NUMBERS = {str(digit): digit for digit in {1, 2, 3, 4, 5, 6, 7, 8, 9}}
NAMES = {
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
}


def main1():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    double_digits = []

    for line in data:
        if not line:
            continue
        leftest, rightest = [-1, None], [-1, None]
        find_first_last(leftest, rightest, line, NUMBERS)
        double_digits.append(10 * leftest[1] + rightest[1])

    print(sum(double_digits))


def get_first_last_occurence(pattern: str, text: str) -> tuple[int, int]:
    x = text.find(pattern)
    y = text.rfind(pattern)
    return x, y


def find_first_last(leftest: list[int], rightest: list[int], text: str, lookup: dict[str, int]) -> tuple[
    list[int], list[int]]:
    for pattern, digit in lookup.items():
        left, right = get_first_last_occurence(pattern, text)
        if left == -1:
            continue
        if leftest[0] == -1 or leftest[0] > left:
            leftest = left, digit
        if rightest[0] == -1 or rightest[0] < right:
            rightest = right, digit
    return leftest, rightest


def main2():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    double_digits = []

    for line in data:
        if not line:
            continue
        leftest, rightest = [-1, None], [-1, None]
        leftest, rightest = find_first_last(leftest, rightest, line, NUMBERS)
        leftest, rightest = find_first_last(leftest, rightest, line, NAMES)

        double_digits.append(10 * leftest[1] + rightest[1])

    print(sum(double_digits))


if __name__ == '__main__':
    main2()
