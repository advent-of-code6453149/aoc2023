range_transition = tuple[int, int, int]
mapping = tuple[str, str, list[range_transition]]


def generate_mapping(text: list[str]) -> list[mapping]:
    mappings = []
    for segment in text:
        if not segment:
            continue
        segment = segment.split('\n')
        key_name, _, value_name = segment[0].split(' ', 1)[0].split('-', 2)
        id_mapping = []
        for line in segment[1:]:
            if not line:
                continue
            dest_range, src_range, range_width = line.split(' ', 2)
            id_mapping.append((int(dest_range), int(src_range), int(range_width)))
        mappings.append((key_name, value_name, id_mapping))
    return mappings


def get_seeds(seed_line: str) -> list[int]:
    seeds = []
    for part in seed_line.split(' '):
        try:
            seeds.append(int(part))
        except ValueError:
            pass
    return seeds


def intersect_intervals(a: tuple[int, int], b: tuple[int, int]) -> tuple[int, int] | None:
    if b[0] > a[1] or a[0] > b[1]:
        return None
    return max(a[0], b[0]), min(a[1], b[1])


def translate_id(mapping: list[range_transition], value: int) -> int:
    for dst_rng, src_rng, rng_width in mapping:
        if src_rng <= value < src_rng + rng_width:
            return dst_rng + value - src_rng
    return value


def complete_mapping(original_mapping: range_transition, doubled_mappings: list[range_transition]
                     ) -> list[tuple[int, int, int]]:
    total_dst_rng, current_index, total_width = original_mapping
    total_src_rng = current_index
    result_mappings = []
    doubled_mappings = sorted(doubled_mappings, key=lambda x: x[1])
    for dst_rng, src_rng, rng_width in doubled_mappings:
        if src_rng > current_index:
            result_mappings.append(
                (current_index - total_src_rng + total_dst_rng, current_index, src_rng - current_index))
        result_mappings.append((dst_rng, src_rng, rng_width))
        current_index = src_rng + rng_width
    if current_index < total_src_rng + total_width:
        result_mappings.append((current_index - total_src_rng + total_dst_rng, current_index,
                                total_src_rng + total_width - current_index))
    return result_mappings


def add_not_yet_applied_mappings(already_applied: list[range_transition], second_to_apply: list[range_transition]
                                 ) -> list[range_transition]:
    to_add = []
    for splittable in sorted(second_to_apply, key=lambda x: x[1]):
        split = split_range(splittable, already_applied)
        for split_part in split:
            for already in already_applied:
                if intersect_intervals((split_part[1], split_part[1] + split_part[2] - 1),
                                       (already[1], already[1] + already[2] - 1)):
                    break
            else:
                to_add.append(split_part)
    return already_applied + to_add


def split_range(range: range_transition, splitters: list[range_transition]) -> list[range_transition]:
    range_dst_rng, range_src_rng, range_rng_width = range
    splits = []
    for splitter in sorted(splitters, key=lambda x: x[1]):
        _, src_rng, rng_width = splitter
        intersect = intersect_intervals((range_src_rng, range_src_rng + range_rng_width - 1),
                                        (src_rng, src_rng + rng_width + 1))
        if not intersect:
            continue
        offset = intersect[0] - range_src_rng
        splits.append((range_dst_rng + offset, range_src_rng + offset, intersect[1] - intersect[0] + 1))
    remaining = []
    last_applied = range_src_rng
    for _, src_rng, _ in sorted(splits, key=lambda x: x[1]):
        offset = last_applied - range_src_rng
        if last_applied < src_rng:
            remaining.append((range_dst_rng + offset, range_src_rng + offset, src_rng - last_applied))
            last_applied = src_rng
    total_segments = remaining + splits
    if last_part := sorted(total_segments, key=lambda x: x[1]):
        dst_rng, src_rng, rng_width = last_part[-1]
        if src_rng + rng_width < range_src_rng + range_rng_width:
            offset = src_rng + rng_width - range_src_rng
            total_segments.append(
                (range_dst_rng + offset,
                 src_rng + rng_width,
                 range_src_rng + range_rng_width - (src_rng + rng_width))
            )
    else:
        total_segments.append(range)
    return total_segments


def combine_mappings(a: mapping, b: mapping) -> mapping:
    if a[1] != b[0]:
        raise TypeError('Incompatible mappings')
    result_mapping = []
    list_a, list_b = sorted(a[2], key=lambda x: x[1]), sorted(b[2], key=lambda x: x[2])
    while list_a:
        first_a = list_a.pop(0)
        part_mappings = []
        for any_b in list_b:
            interval = intersect_intervals((first_a[0], first_a[0] + first_a[2] - 1),
                                           (any_b[1], any_b[1] + any_b[2] - 1))
            if not interval:
                continue
            src_int_dist = interval[0] - first_a[0]
            dst_int_dist = interval[0] - any_b[1]
            new_src_rng = src_int_dist + first_a[1]
            new_dst_rng = dst_int_dist + any_b[0]
            new_rng_width = interval[1] - interval[0] + 1
            part_mappings.append((new_dst_rng, new_src_rng, new_rng_width))
        result_mapping.extend(complete_mapping(first_a, part_mappings))
    result_mapping = add_not_yet_applied_mappings(result_mapping, list_b)
    result_mapping = sorted(result_mapping, key=lambda x: x[1])
    return a[0], b[1], result_mapping


def translate_id_range(translation: list[range_transition], id_range: tuple[int, int]) -> list[tuple[int, int]]:
    range_src_rng, range_rng_width = id_range
    translation = sorted(translation, key=lambda x: x[1])
    last_mapped = -1
    ranges = []
    for dst_rng, src_rng, rng_width in translation:
        intersect = intersect_intervals((range_src_rng, range_src_rng + range_rng_width - 1),
                                        (src_rng, src_rng + rng_width - 1))
        if not intersect:
            continue
        if last_mapped == -1 and range_src_rng < src_rng:
            ranges.append((range_src_rng, src_rng - range_src_rng))
        elif last_mapped != -1 and last_mapped < src_rng:
            ranges.append((last_mapped, src_rng - last_mapped))
        offset = intersect[0] - src_rng
        ranges.append((dst_rng + offset, intersect[1] - intersect[0] + 1))
        last_mapped = src_rng + ranges[-1][1] + offset
    return ranges


def main1():
    with open('data.txt') as fh:
        data = fh.read().split('\n\n')
    values = get_seeds(data[0])
    current_name = 'seed'
    seed_mappings = generate_mapping(data[1:])
    while current_name != 'location':
        _, to_name, translation = next(mapping for mapping in seed_mappings if mapping[0] == current_name)
        current_name = to_name
        values = [translate_id_range(translation, value) for value in values]
    print(min(values))


def main2():
    with open('data.txt') as fh:
        data = fh.read().split('\n\n')
    old_values = get_seeds(data[0])
    values = []
    for index in range(len(old_values) // 2):
        values.append((old_values[2 * index], old_values[2 * index + 1]))
    current_name = 'seed'
    seed_mappings = generate_mapping(data[1:])
    while len(seed_mappings) > 1:
        for part_id, part_mapping in enumerate(seed_mappings[1:]):
            combined = combine_mappings(seed_mappings[0], part_mapping)
            seed_mappings.remove(part_mapping)
            seed_mappings.remove(seed_mappings[0])
            seed_mappings.insert(0, combined)
    while current_name != 'location':
        _, to_name, translation = next(mapping for mapping in seed_mappings if mapping[0] == current_name)
        current_name = to_name
        old_values = values
        values = []
        for value in old_values:
            values.extend(translate_id_range(translation, value))
    print(min(values)[0])


if __name__ == '__main__':
    main2()
