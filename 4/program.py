def main1():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    lines = [extract_numbers(line)[1:] for line in data if line]
    lines = [get_points(*line) for line in lines]
    print(sum(lines))


def extract_numbers(line: str) -> tuple[int, tuple[int], tuple[int]]:
    line_index, line = line.split(':', 1)
    line_index = int(line_index.split(' ')[-1])
    winning, actual = set(), set()
    winning_part, actual_part = line.split('|', 1)
    for index in range(len(winning_part) // 3):
        number = winning_part[3 * index: 3 * (index + 1)]
        number = int(number)
        winning.add(number)
    for index in range(len(actual_part) // 3):
        number = actual_part[3 * index: 3 * (index + 1)]
        number = int(number)
        actual.add(number)
    return line_index, tuple(sorted(winning)), tuple(sorted(actual))


def get_points(winning_numbers: set[int], actual_numbers: set[int]) -> int:
    matches = sum(number in winning_numbers for number in actual_numbers)
    if matches == 0:
        return 0
    return 2 ** (matches - 1)


def get_matches(winning_numbers: tuple[int], actual_numbers: tuple[int]) -> int:
    return sum(number in winning_numbers for number in actual_numbers)


def main2():
    with open('data.txt') as fh:
        data = fh.read().split('\n')
    lines = [extract_numbers(line) for line in data if line]
    copies = {line_id + 1: 1 for line_id in range(len(lines))}
    for line_id, winning, actual in lines:
        matches = get_matches(winning, actual)
        for more_line_id in range(matches):
            copies[line_id + more_line_id + 1] += copies[line_id]
    print(sum(copies.values()))


if __name__ == '__main__':
    main2()
